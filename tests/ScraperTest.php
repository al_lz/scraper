<?php

use PHPUnit\Framework\TestCase;

class ScraperTest extends TestCase
{

    protected function setUp(): void
    {
    }
    
    function testProductsGettersSetters(){
        
        $product1 = new Product('one product');
        $product1->setDescription("one");
        $product1->setPrice("one");
        $product1->setDiscount("one");
        $this->assertSame('one product' ,$product1->getTitle());
        $this->assertSame('one' ,$product1->getDescription());
        $this->assertSame('one' ,$product1->getPrice());
        $this->assertSame('one' ,$product1->getDiscount());
    }

    function testProducts(){
        
        $products = [];
        $product1 = new Product('one product');
        $product2 = new Product('second product');
        array_push($products, $product1);
        array_push($products, $product2);
        $this->assertCount(2 ,$products);
    }

    function testScraper()
    {
        $scraper = new Scraper();
        $this->assertIsObject($scraper);
    }

    function testScraperResult()
    {
        $scraper = new Scraper();
        $products =$scraper->getScrapedProducts();
        $this->assertCount(6, $products);

        foreach($products as $product){
            $this->assertObjectHasAttribute('title', $product);
            $this->assertObjectHasAttribute('description', $product);
            $this->assertObjectHasAttribute('price', $product);
            $this->assertObjectHasAttribute('discount', $product);
        }
    }
}
