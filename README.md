## Instalation

Clone repo.  

`git clone https://gitlab.com/al_lz/scraper.git`

Cd into folder "scraper".

Install dependencies:

`composer install`

Execute app (php needs to be installed):

`./app products`


## Test

From root directory:  

`vendor/bin/phpunit --color tests/`