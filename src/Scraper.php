<?php

use Goutte\Client;

class Scraper 

{
    protected $products;
    protected $client;
    protected $crawler;

    const URL = "https://videx.comesconnected.com/";

    public function __construct()
    {
        $this->products = array();
        $this->client = new Client();
        $this->crawler = $this->client->request('GET', self::URL);    
    }


    public function getScrapedProducts(){

            // $this->obj->options = array();
        
            $out = $this->crawler->filter('.package .header h3')->each(function ($node) {
                $product = new Product($node->text());
                array_push($this->products, $product);
            });
        
            foreach($this->products as $key => $product){
                $this->crawler->filter('.package .package-features ul')->eq($key)->filter('li')->each(function ($node,$i) use ($product) {
                    switch($i){
                        case 0:
                            $product->setDiscount($node->text());
                            break;
                        case 1:
                            $discount = explode('Save', $node->text());
                            $product->setPrice(($discount)? trim($discount[0]):$discount);
                            $product->setDiscount((sizeof($discount) > 1)? trim($discount[1]):false);
                            break;
                    
                    }
                });

            }
            // var_dump($this->products);
             $this->products = array_reverse($this->products);
             return $this->products;
    }



                

}