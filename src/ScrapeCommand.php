<?php

use Goutte\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScrapeCommand extends Command

{

    protected function configure(){

        $this->setName('products')
            ->setDescription('Scrape product info from videx');
    
        }

        protected function execute(InputInterface $input, OutputInterface $output){

            $scraper = new Scraper();
            $output->writeln(json_encode($scraper->getScrapedProducts()));
            return Command::SUCCESS;

        }
}